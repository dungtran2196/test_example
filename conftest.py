# -*- coding: utf-8 -*-

import os.path
import pathlib
from typing import Final
import pytest
import allure
from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.firefox.service import Service

IMPLICITLY_WAIT: Final[int] = 30
GECKODRIVER_PATH: Final[str] = os.path.join(pathlib.Path(__file__).parent.absolute(), "geckodriver")
service = Service(executable_path=GECKODRIVER_PATH)
options = webdriver.FirefoxOptions()
options.set_capability("browserName", "firefox")
options.set_capability("acceptInsecureCerts", True)
options.set_capability("debuggerAddress", True)


def pytest_addoption(parser):
    """
    If we use the '--remote' flag, tests will try to be run against a Docker container on port 4444
    :param parser:
    :return:
    """
    parser.addoption(
        "--remote", action="store_true", default=False,
        help="Set this flag if the driver you are using is not on the local system"
    )
    parser.addoption(
        "--rmt-host", action="store", default="localhost", type=str,
        help="Set the host name or the host IP of the system who contains the web driver. By default 'localhost'"
    )
    parser.addoption(
        "--rmt-port", action="store", default=4444, type=int,
        help="Set the port where the host who has the driver is listening. By default '4444'"
    )

@pytest.hookimpl(hookwrapper=True, tryfirst=True)
def pytest_runtest_makereport(item):
    outcome = yield
    rep = outcome.get_result()
    setattr(item, "rep_" + rep.when, rep)
    return rep

@allure.step("Open Browser")
@pytest.fixture(scope="function")
def open_browser(request):
    """
    Generate the Selenium driver that will be used by the tests
    :param request:
    :return: a callable that generates the driverSelenium WebDriver instance
    """
    if request.config.getoption("--remote"):
        driver = webdriver.Remote(
            command_executor="http://{host}:{port}/wd/hub".format(
                host=request.config.getoption("--rmt-host"), port=request.config.getoption("--rmt-port")
            ), options=options
        )
    else:
        driver = webdriver.Firefox(service=service)
    driver.implicitly_wait(IMPLICITLY_WAIT)
    driver.maximize_window()

    yield driver
    driver.close()
    driver.quit()

@pytest.fixture(autouse=True)
def allure_logs(request, open_browser):
    driver = open_browser
    yield driver
    if request.node.rep_call.failed:
        # Make the screen-shot if test failed:
        try:
            driver.execute_script("document.body.bgColor = 'white';")
            allure.attach(driver.get_screenshot_as_png(),
                          name=request.function.__name__,
                          attachment_type=allure.attachment_type.PNG)
        except:
            pass # just ignore
