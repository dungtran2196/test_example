# "sb" pytest fixture test in a method with no class
def test_sb_fixture(open_browser):
    open_browser.open("seleniumbase.io/help_docs/install/")
    open_browser.type('input[aria-label="Search"]', "GUI Commander")
    open_browser.click('mark:contains("Commander")')
    open_browser.assert_title_contains("GUI / Commander")


